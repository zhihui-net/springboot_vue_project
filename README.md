# springboot_vue_project

#### 介绍
springboot+vue搭建人事管理系统


#### 技术栈

后端技术
springboot +mybatis+mybatis-plus

前端技术
Vue3.0 + Vue-Router + Vuex + Axios + ElementPlus 

#### 开发环境

JDK： jdk8

mysql：mysql-5.7

IDE：IntelliJ IDEA 2021、VSCode



下载项目：

```
git clone git@gitee.com:zhihui-net/springboot_vue_project.git
```

前端项目

```
1、npm install

2、npm run serve
```

后端

配置自己的邮箱和数据库，运行引导类。
