module.exports = {
    devServer: {
        // 端口号
        port: 8080,
        // 配置不同的后台API地址
        client: {
            webSocketURL: 'ws://0.0.0.0:8080/ws',
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
        }
    }
}
