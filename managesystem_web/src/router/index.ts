import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import Login from '../views/Login.vue'
import Register from "@/views/Register.vue";
//找回密码
import FindPassword from "@/views/FindPassword.vue";

// 映射页面
import PageOne from "../views/PageOne.vue";
import AddUserInfo from "../views/AddUserInfo.vue"
import Erro404 from "@/views/Erro404.vue";
import FindEdu from "@/views/edu/FindEdu.vue";
import AddEdu from "@/views/edu/AddEdu.vue";
import FindWork from "@/views/work/FindWork.vue";
import AddWork from "@/views/work/AddWork.vue";
import FindHy from "@/views/hy/FindHy.vue";
import AddHy from "@/views/hy/AddHy.vue";
import FindOther from "@/views/other/FindOther.vue";
import AddOther from "@/views/other/AddOther.vue";
import Update from "@/views/Update.vue";


const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'login',
        component: Login
    },
    {
        path: '/home',
        name: 'home',
        component: HomeView,
        // 设置首页必须登录才能点击
        meta: {
            isLogin: true
        },
        //设置二级路由
        children: [
            {
                path: '/pageOne',
                component: PageOne
            },
            {
                path: '/addUserInfo',
                component: AddUserInfo
            },
            //教育经历
            {
                path: '/findEdu',
                name: 'findEdu',
                component: FindEdu,
            },
            {
                path: '/addEdu',
                name: 'addEdu',
                component: AddEdu,
            },
            //工作经历
            {
                path: '/findWork',
                name: 'findWork',
                component: FindWork,
            },
            {
                path: '/addWork',
                name: 'addWork',
                component: AddWork,
            },
            //健康情况
            {
                path: '/findHy',
                name: 'findHy',
                component: FindHy,
            },
            {
                path: '/addHy',
                name: 'addHY',
                component: AddHy,
            },
            //其他情况
            {
                path: '/findOther',
                name: 'findOther',
                component: FindOther,
            },
            {
                path: '/addOther',
                name: 'addOther',
                component: AddOther,
            }
        ]
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/findPassword',
        name: 'findPassword',
        component: FindPassword
    },
    {
        path: '/erro404',
        component: Erro404
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router
