import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
//引入axios组件
import axios from 'axios';
import VueAxios from 'vue-axios'
//引入element-plus组件
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';

// @ts-ignore
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'

createApp(App).use(ElementPlus,{locale: zhCn}).use(VueAxios,axios).use(store).use(router).mount('#app')
