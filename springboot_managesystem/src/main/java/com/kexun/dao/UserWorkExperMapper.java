package com.kexun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kexun.entity.UserWorkExperPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserWorkExperMapper extends BaseMapper<UserWorkExperPO> {
}
