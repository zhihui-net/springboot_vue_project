package com.kexun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kexun.entity.UserEduExperPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserEduExperMapper extends BaseMapper<UserEduExperPO> {
}
