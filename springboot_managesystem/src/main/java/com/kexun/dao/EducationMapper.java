package com.kexun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kexun.entity.UserEduExperPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description: 教育经历表
 * date: 2023/1/23 11:41

 * @author sq
 * @since JDK 1.8
 */
@Mapper
public interface EducationMapper extends BaseMapper<UserEduExperPO> {

}
