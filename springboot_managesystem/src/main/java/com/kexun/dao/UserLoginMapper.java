package com.kexun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kexun.entity.UserLoginPO;
import org.apache.ibatis.annotations.Mapper;

/**
 * Description:
 * date 2023/1/15 15:19
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@Mapper
public interface UserLoginMapper extends BaseMapper<UserLoginPO> {
}
