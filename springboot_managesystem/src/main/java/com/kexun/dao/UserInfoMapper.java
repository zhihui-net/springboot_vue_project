package com.kexun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kexun.entity.UserInfoPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Description:
 * date 2023/1/15 14:01
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfoPO> {
}
