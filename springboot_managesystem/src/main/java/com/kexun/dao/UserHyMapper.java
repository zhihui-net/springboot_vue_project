package com.kexun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kexun.entity.UserHyPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserHyMapper extends BaseMapper<UserHyPO> {
}
