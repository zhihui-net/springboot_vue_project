package com.kexun.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kexun.entity.UserOtherMesgPO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserOtherMesgMapper extends BaseMapper<UserOtherMesgPO> {
}
