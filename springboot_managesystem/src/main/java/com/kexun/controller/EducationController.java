package com.kexun.controller;

import com.kexun.entity.UserEduExperPO;
import com.kexun.service.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EducationController {
    @Autowired
    EducationService educationService;

    /**
     * Description: 添加教育经历表内容
     * date: 2023/1/23 11:46
     *
     * @author sq
     * @since JDK 1.8
     */
    @RequestMapping("/education")
    public void addEdu(@RequestBody UserEduExperPO educationPO){
        int insert = educationService.getBaseMapper().insert(educationPO);
        System.out.println(insert);
    }

}
