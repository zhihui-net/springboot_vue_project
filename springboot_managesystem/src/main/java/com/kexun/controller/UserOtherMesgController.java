package com.kexun.controller;

import com.kexun.entity.UserOtherMesgPO;
import com.kexun.service.UserOtherMesgService;
import com.kexun.utils.Result3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**						
* Description: 用户其他信息						
* date 2023/2/5 13:06
* @author SunQiang					
* @since JDK 1.8						
*/	
@RestController
@RequestMapping(value="/other")
public class UserOtherMesgController {
    @Autowired
    private UserOtherMesgService userOtherMesgService;

    /**
     * Description: 查询所有用户其他信息
     * date: 2023/2/4 21:42
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping
    public Result3Utils getMethodOther() {
        Result3Utils result3Utils = new Result3Utils();
        result3Utils.setFlag(true);
        List<UserOtherMesgPO> userOtherMesgPOS = userOtherMesgService.getBaseMapper().selectList(null);
        result3Utils.setData(userOtherMesgPOS);
        return result3Utils;
    }

    /**
     * Description: 根据条件查询单个用户其他信息
     * date: 2023/2/4 21:43
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping("/id")
    public Result3Utils getMethodByIdOther(String userId) {
        Result3Utils result3Utils = new Result3Utils();
        result3Utils.setFlag(true);
        Map<String, Object> map = new HashMap<>();
        map.put("user_id", userId);
        List<UserOtherMesgPO> userOtherMesgPOS = userOtherMesgService.getBaseMapper().selectByMap(map);
        result3Utils.setData(userOtherMesgPOS);
        return result3Utils;
    }
}
