/*
*Copyright (c) 2022 nanjing kexun IT co.ltd.南京科迅科技有限公司
*/
package com.kexun.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Description:  controller层的全局异常处理
 * date 2023/1/15 14:21
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@ControllerAdvice
public class GlobalException {
    @ExceptionHandler(value =Exception.class)
    @ResponseBody
    public String exceptionHandler(Exception e){
        System.out.println("全局异常捕获>>>:"+e);
        return "全局异常捕获,错误原因>>>"+e.getMessage();
    }
}
