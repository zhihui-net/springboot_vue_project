package com.kexun.controller;

import com.kexun.entity.UserHyPO;
import com.kexun.entity.UserWorkExperPO;
import com.kexun.service.UserHyService;
import com.kexun.utils.Result3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description: 用户健康情况
 * date 2023/2/5 12:07
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@RestController
@RequestMapping(value = "/hys")
public class UserHyController {
    @Autowired
    private UserHyService userHyService;

    /**
     * Description: 查询所有用户健康信息
     * date: 2023/2/4 21:42
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping
    public Result3Utils getMethods() {
        Result3Utils result3Utils = new Result3Utils();
        result3Utils.setFlag(true);
        List<UserHyPO> userHyPOS = userHyService.getBaseMapper().selectList(null);
        result3Utils.setData(userHyPOS);
        return result3Utils;
    }

    /**
     * Description: 根据条件查询单个用户健康信息
     * date: 2023/2/4 21:43
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping("/id")
    public Result3Utils getMethodByIds(String userId) {
        Result3Utils result3Utils = new Result3Utils();
        result3Utils.setFlag(true);
        Map<String, Object> map = new HashMap<>();
        map.put("user_id", userId);
        List<UserHyPO> userHyPOS = userHyService.getBaseMapper().selectByMap(map);
        result3Utils.setData(userHyPOS);
        return result3Utils;
    }

}
