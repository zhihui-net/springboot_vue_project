/*
 *Copyright (c) 2022 nanjing kexun IT co.ltd.南京科迅科技有限公司
 */
package com.kexun.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kexun.entity.UserInfoPO;
import com.kexun.service.UserInfoService;
import com.kexun.utils.ResultUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Description:  用户信息
 * date 2023/1/15 14:11
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@RestController
@RequestMapping(value = "/userInfo", produces = "application/json; charset=utf-8")
public class UserInfoController {
    @Autowired
    private UserInfoService userInfoService;

    @Resource
    private RestHighLevelClient restHighLevelClient;

    /**
     * Description: 查询所有
     * date: 2023/1/15 14:13
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping
    public ResultUtils searchUser() {
        ResultUtils resultUtils = new ResultUtils();
        resultUtils.setFlag(true);
        List<UserInfoPO> userInfoPOS = userInfoService.getBaseMapper().selectList(null);
        System.out.println(userInfoPOS);
        resultUtils.setData(userInfoPOS);
        return resultUtils;
    }

    /**
     * Description: 根据条件查询(结合ES)
     * date: 2023/1/15 14:13
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping("/user")
    public ResultUtils searchUserById(@RequestParam String name) throws IOException {
        ResultUtils resultUtils = new ResultUtils();
        resultUtils.setFlag(true);

        SearchRequest searchRequest = new SearchRequest("demo");
        //构造搜索条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //查询条件，我们可以使用QueryBuilders工具类
        MatchQueryBuilder names = QueryBuilders.matchQuery("name", name);
        searchSourceBuilder.query(names);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        searchRequest.source(searchSourceBuilder);

        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        System.out.println(JSON.toJSONString(search.getHits()));
        System.out.println("==========");
        //创建一个List<>存放查询结果的对象
        ArrayList<Object> list = new ArrayList<>();
        for (SearchHit document : search.getHits().getHits()) {
            //getSourceAsString()返回结果为json格式
            JSONObject jsonObject = JSONObject.parseObject(document.getSourceAsString());
            list.add(jsonObject);
            // 把创建的集合set进去
            resultUtils.setData(list);
            System.out.println(document.getSourceAsString());
        }
        return resultUtils;
    }

    /**
     * Description:  添加用户
     * date: 2023/1/15 14:33
     *
     * @author sq
     * @since JDK 1.8
     */
    @PostMapping
    public ResultUtils addUser(@RequestBody UserInfoPO userInfoPO) {
        ResultUtils resultUtils = new ResultUtils();
        resultUtils.setFlag(true);
        boolean save = userInfoService.save(userInfoPO);
        if (save == true) {
            System.out.println("添加成功...");
        } else {
            System.out.println("添加失败...");
        }
        return resultUtils;
    }

    /**
     * Description:  更新用户
     * date: 2023/1/15 14:33
     * @author sq
     * @since JDK 1.8
     */
/*    @PutMapping
    public ResultUtils updateUser(@RequestBody UserInfoPO userInfoPO){
        ResultUtils resultUtils = new ResultUtils();
        resultUtils.setFlag(true);
        boolean b = userInfoService.updateById(userInfoPO);
        if(b==true){
            System.out.println("更新成功...");
        }else{
            System.out.println("更新失败...");
        }
        return resultUtils;
    }*/

    /**
     * Description:  删除用户
     * date: 2023/1/15 14:33
     *
     * @author sq
     * @since JDK 1.8
     */
    @DeleteMapping
    public ResultUtils deleteUser(Integer id) {
        ResultUtils resultUtils = new ResultUtils();
        resultUtils.setFlag(true);
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", id);
//        int i = userInfoService.getBaseMapper().deleteByMap(map);
        int i = userInfoService.getBaseMapper().deleteById(id);
        if (i == 1) {
            System.out.println("删除成功...");
        } else {
            System.out.println("删除失败...");
        }
        return resultUtils;
    }

    /**
     * Description: excel表格数据下载导出
     * date: 2023/2/5 15:14
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping("/excel")
    //  todo 下载目录在E盘ExcelDown
    public void getExcel() {
        List<UserInfoPO> list = userInfoService.getBaseMapper().selectList(null);
        String PATH = "E:\\ExcelDown\\";
        //下载到本地文件的路径
        String fileName = PATH + "EastExcel.xlsx";
        //write:指定UserInfoPO.class去写数据
        //sheet名称
        //doWrite数据查询到的数据
        EasyExcel.write(fileName,UserInfoPO.class).sheet("模板").doWrite(list);
    }
}
