package com.kexun.controller;

import com.kexun.entity.UserEduExperPO;
import com.kexun.entity.UserWorkExperPO;
import com.kexun.service.UserWorkExperService;
import com.kexun.utils.Result3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* Description: 用户工作经历
* date 2023/2/5 11:55
* @author SunQiang
* @since JDK 1.8
*/
@RestController
@RequestMapping(value = "/work")
public class UserWorkExperController {
    @Autowired
    private UserWorkExperService userWorkExperService;

    /**
     * Description: 查询所有用户工作经历
     * date: 2023/2/4 21:42
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping
    public Result3Utils getMethod() {
        Result3Utils result3Utils = new Result3Utils();
        result3Utils.setFlag(true);
        List<UserWorkExperPO> userWorkExperPOS = userWorkExperService.getBaseMapper().selectList(null);
        result3Utils.setData(userWorkExperPOS);
        return result3Utils;
    }

    /**
     * Description: 根据条件查询单个用户工作经历
     * date: 2023/2/4 21:43
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping("/id")
    public Result3Utils getMethodById(String userId) {
        Result3Utils result3Utils = new Result3Utils();
        result3Utils.setFlag(true);
        Map<String, Object> map = new HashMap<>();
        map.put("user_id", userId);
        List<UserWorkExperPO> userWorkExperPOS = userWorkExperService.getBaseMapper().selectByMap(map);
        result3Utils.setData(userWorkExperPOS);
        return result3Utils;
    }

}
