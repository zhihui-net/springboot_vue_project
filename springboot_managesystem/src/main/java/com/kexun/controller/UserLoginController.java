/*
 *Copyright (c) 2022 nanjing kexun IT co.ltd.南京科迅科技有限公司
 */
package com.kexun.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.kexun.entity.UserLoginPO;
import com.kexun.entity.VerticalVO;
import com.kexun.service.SendMailService;
import com.kexun.service.UserLoginService;
import com.kexun.utils.MD5Utils;
import com.kexun.utils.SatokenInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Description:  用户登录
 * date 2023/1/15 15:31
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@RestController
@RequestMapping(value = "/user", produces = "text/html; charset=utf-8")
public class UserLoginController {
    /**
     * 定义一个全局变量用于接收随机验证码结果
     */
    public String rNumber = "";

    /**
     * 注入redistemplate
     */
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    UserLoginService userLoginService;

    /**
     * 注入captchaProducer图片随机验证码
     */
    @Autowired
    private Producer captchaProducer;

    @Autowired
    private SendMailService sendMailService;

    /**
     * Description: 图片随机验证密码生成
     * date: 2023/1/27 11:40
     *
     * @author sq
     * @since JDK 1.8
     */
    @RequestMapping("/kaptcha")
    public void getKaptchaImage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");
        //生成验证码
        String capText = captchaProducer.createText();
        //将生成的随机验证码赋值给全局变量，以便随后登录校验
        rNumber = capText;
        session.setAttribute(Constants.KAPTCHA_SESSION_KEY, capText);
        //向客户端写出
        BufferedImage bi = captchaProducer.createImage(capText);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(bi, "jpg", out);
        try {
            out.flush();
        } finally {
            out.close();
        }
    }

    /**
     * Description: 登录判断，添加带有token返回值
     * date: 2023/1/22 0:24
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping(value = "/login", produces = "application/json; charset=utf-8")
    public SatokenInfo loginStatus(HttpServletRequest req) {
        //获取参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        //加密密码，以便将密码与数据库进行对比
        String code = MD5Utils.code(password);

        //获取随机验证码
        String number = req.getParameter("number");

        Map<String, Object> map = new HashMap<>();
        map.put("username", username);
        //将加密后的密码出入map
        map.put("password", code);
        List<UserLoginPO> userLoginPOS = userLoginService.getBaseMapper().selectByMap(map);

        //获取token信息
        SatokenInfo satokenInfo = new SatokenInfo();
        satokenInfo.setStatus(200);
        //这个login是登陆(这个原理暂时不清楚)
        StpUtil.login(username);

        //获取该用户是否为管理员，否则抛出异常
        StpUtil.checkRole("管理员");
        boolean role = StpUtil.hasRole("管理员");
        System.out.println(role);

        //判断是否为空，判断随机验证码是否符合条件
        if (!userLoginPOS.isEmpty() && rNumber.equals(number)) {
            System.out.println(rNumber);
            System.out.println(number);
            System.out.println("登录成功");
            satokenInfo.setToken(StpUtil.getTokenValue());
            satokenInfo.setLogin(true);
            System.out.println(satokenInfo);
            return satokenInfo;
        } else {
            System.out.println("登录失败");
            satokenInfo.setLogin(false);
            System.out.println(satokenInfo);
            return satokenInfo;
        }
    }

    /**
     * Description: 发送邮件验证码
     * date: 2023/1/30 14:58
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping(value = "/mailSend", produces = "application/json; charset=utf-8")
    public SimpleMailMessage send(String mail) {
        SimpleMailMessage simpleMailMessage = sendMailService.sendMail(mail);
        //取出随机验证码，存放到数据库
        ValueOperations ops = stringRedisTemplate.opsForValue();
        //设置过期时间为1分钟,获取邮件中的验证码部分存入缓存
        ops.set(mail, simpleMailMessage.getText().substring(18, 24), 30, TimeUnit.SECONDS);
        Object age = ops.get(mail);
        System.out.println("验证码：" + age);
        return simpleMailMessage;
    }


    /**
     * Description: 用户注册功能
     * date: 2023/1/19 9:42
     *
     * @author sq
     * @since JDK 1.8
     * 由于post参数再传递过程中会将实体中的参数放到body里面，
     * 单独的验证码建议封装到一个独立的实体当中。
     */
    @PostMapping
    public String addUser(@RequestBody VerticalVO verticalVO) {
        //设置密文保存密码
        String code = MD5Utils.code(verticalVO.getPassword());

        UserLoginPO userLoginPO = new UserLoginPO();
        userLoginPO.setPassword(code);
        userLoginPO.setTelNumber(verticalVO.getTelNumber());
        userLoginPO.setUsername(verticalVO.getUsername());

        ValueOperations ops = stringRedisTemplate.opsForValue();
        //取出redis中的验证码
        Object verifyCode = ops.get(verticalVO.getMail());
        //输出数据库验证码存储的结果
        System.out.println(verifyCode);

        System.out.println(verticalVO.getVcode());
        //如果输入的验证码和发送的随机数字相等并且数据库验证码没有过期则注册成功
        if (verticalVO.getVcode().equals(verifyCode) && verifyCode != null) {
            //如果验证码符合就进行插入
            userLoginService.getBaseMapper().insert(userLoginPO);
            return "注册成功";
        }else {
            return "该验证码已过期或验证码不存在";
        }
    }

    /**
     * Description: 用户找回密码
     * date: 2023/1/19 9:42
     *
     * @author sq
     * @since JDK 1.8
     */
    @PutMapping
    public String findPassword(@RequestBody UserLoginPO userLoginPO) {
        UpdateWrapper<UserLoginPO> updateWrapper = new UpdateWrapper<>();
        UserLoginPO userLoginPO1 = new UserLoginPO();
        //加密密码
        String code = MD5Utils.code(userLoginPO.getPassword());
        //将用户输入的密码作为更新条件
        userLoginPO1.setPassword(code);
        System.out.println(userLoginPO1.getPassword());
        UpdateWrapper<UserLoginPO> username = updateWrapper.eq("username", userLoginPO.getUsername());

        Integer update = null;
        if (userLoginPO1 != null) {
            update = userLoginService.getBaseMapper().update(userLoginPO1, username);
        }
        if (update == 1) {
            return "更新密码成功";
        } else {
            System.out.println("更新密码失败");
            return "更新密码失败";
        }
    }

    /**
     * Description: 查询登录用户
     * date: 2023/1/29 16:55
     * @author sq
     * @since JDK 1.8
     */
    //todo 查询登录用户功能

}
