package com.kexun.controller;

import com.kexun.entity.UserEduExperPO;
import com.kexun.service.UserEduExperService;
import com.kexun.utils.Result3Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**						
* Description: 教育经历						
* date 2023/2/5 12:53
* @author SunQiang					
* @since JDK 1.8						
*/	
@RestController
@RequestMapping(value = "/edu", produces = "application/json; charset=utf-8")
public class UserEduExperController {
    @Autowired
    private UserEduExperService userEduExperService;

    /**
     * Description: 查询所有教育经历
     * date: 2023/2/4 21:42
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping
    public Result3Utils getMethod() {
        Result3Utils result3Utils = new Result3Utils();
        result3Utils.setFlag(true);
        List<UserEduExperPO> userEduExperPOS = userEduExperService.getBaseMapper().selectList(null);
        result3Utils.setData(userEduExperPOS);
        return result3Utils;
    }

    /**
     * Description: 根据条件查询
     * date: 2023/2/4 21:43
     *
     * @author sq
     * @since JDK 1.8
     */
    @GetMapping("/id")
    public Result3Utils getMethodById(String userId) {
        Result3Utils result3Utils = new Result3Utils();
        result3Utils.setFlag(true);
        Map<String, Object> map = new HashMap<>();
        map.put("user_id", userId);
        List<UserEduExperPO> userEduExperPOS = userEduExperService.getBaseMapper().selectByMap(map);
        result3Utils.setData(userEduExperPOS);
        return result3Utils;
    }

    /**
     * Description: 添加方法
     * date: 2023/2/4 21:49
     *
     * @author sq
     * @since JDK 1.8
     */
    @PostMapping
    public Result3Utils addMethod(@RequestBody UserEduExperPO userEduExperPO) {
        Result3Utils resultUtils = new Result3Utils();
        resultUtils.setFlag(true);
        boolean save = userEduExperService.save(userEduExperPO);
        if (save == true) {
            resultUtils.setData("插入成功");
        } else {
            resultUtils.setData("插入失败");
        }
        return resultUtils;
    }

    /**
     * Description: 删除用户教育经历
     * date: 2023/2/5 11:02
     *
     * @author sq
     * @since JDK 1.8
     */
    @DeleteMapping
    public Result3Utils delMethod(String eduId) {
        Result3Utils resultUtils = new Result3Utils();
        resultUtils.setFlag(true);
        HashMap<String, Object> map = new HashMap<>();
        map.put("edu_id", eduId);
        int i = userEduExperService.getBaseMapper().deleteByMap(map);
        if (i == 1) {
            resultUtils.setData("删除成功");
        } else {
            resultUtils.setData("删除失败");
        }
        return resultUtils;
    }

    /**
     * Description: 用户更新
     * date: 2023/2/5 11:28
     *
     * @author sq
     * @since JDK 1.8
     */
    @PutMapping
    public Result3Utils updateMethod(@RequestBody UserEduExperPO userEduExperPO) {
        Result3Utils resultUtils = new Result3Utils();
        resultUtils.setFlag(true);
        int i = userEduExperService.getBaseMapper().updateById(userEduExperPO);
        if (i == 1) {
            resultUtils.setData("更新成功");
        } else {
            resultUtils.setData("更新失败");
        }
        return resultUtils;
    }
}
