/*
 *Copyright (c) 2022 nanjing kexun IT co.ltd.南京科迅科技有限公司
 */
package com.kexun.config;

import org.springframework.context.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Description: spring核心配置类
 * date 2022/12/10 11:24
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@Configuration
@ComponentScan(value = "com.kexun",
        excludeFilters = @ComponentScan.Filter(
                type = FilterType.ANNOTATION,
                classes = Controller.class
        ))
@EnableWebMvc
//@Import({ShiroConfig.class})
public class SpringConfig {

}
