/*
*Copyright (c) 2022 nanjing kexun IT co.ltd.南京科迅科技有限公司
*/
package com.kexun.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description:  MP配置类
 * date 2022/12/29 15:48
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@Configuration
public class MPConfig {

    /**
     * Description: 分页拦截器
     * date 2022/12/29 15:52
     *
     * @return MybatisPlusInterceptor 分页拦截器
     * @author SunQiang
     * @since JDK 1.8
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }
}
