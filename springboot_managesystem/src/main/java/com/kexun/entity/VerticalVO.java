package com.kexun.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerticalVO {
    private String password;
    private String username;
    private String telNumber;
    private String vcode;
    private String mail;

}
