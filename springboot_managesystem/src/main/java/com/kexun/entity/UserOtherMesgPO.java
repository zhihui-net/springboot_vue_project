package com.kexun.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Description: 其他情况说明
 * date: 2023/1/27 14:21

 * @author sq
 * @since JDK 1.8
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_other_mesg")
public class UserOtherMesgPO {
    private Integer otId;
    private String userId;
    private String englishAbility;
    private String otherLanguage;
    private String otherLanguageAbility;
    private String requireComputer;
    private String obtainEmployment;
    private String arrangeAccommodation;
    private String registrationAttachment;
    private String signatureOfTrainees;
    private Date  fillingTime;
    private Date createTime;
    private Date updateTime;
}
