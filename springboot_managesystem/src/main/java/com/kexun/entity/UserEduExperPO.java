package com.kexun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Description: 教育经历情况                        
 * date: 2023/1/27 14:25
                         
 * @author sq
 * @since JDK 1.8                        
 */   
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("user_edu_exper")
@Component
public class UserEduExperPO {
    @TableId(value="edu_id",type = IdType.AUTO)
    private Integer eduId;
    private String userId;
    private Date startDate;
    private Date endDate;
    private String schoolOrInstitutions;
    private String major;
    private String educationalLevel;
    private Date createTime;
    private Date updateTime;
}
