package com.kexun.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("user_hy")
public class UserHyPO {
    private Integer hyId;
    private String userId;
    private String hyDescription;
    private String illnessStatus;
    private String illnessType;
    private Date createTime;
    private Date updateTime;
}
