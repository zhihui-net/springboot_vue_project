package com.kexun.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Description:   工作经历情况
 * date: 2023/1/27 14:21

 * @author sq
 * @since JDK 1.8
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_work_exper")
public class UserWorkExperPO {
    private Integer workId;
    private String userId;
    private Date startTime;
    private Date endTime;
    private String company;
    private String major;
    private String aboutCourse;
    private Date createTime;
    private Date updateTime;
}
