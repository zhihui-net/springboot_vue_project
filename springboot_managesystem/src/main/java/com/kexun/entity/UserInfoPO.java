package com.kexun.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Description: 用户基本信息
 * date 2023/1/15 13:54
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Component
@TableName("user_info")
public class UserInfoPO {
    @TableId(value="id",type = IdType.AUTO)
//    @ExcelProperty(value = {"随机编号"}, index = 1)
    /**随机编号*/
    private Integer id;
    /** 用户编号*/
    @ExcelProperty(value = {"学员编号"}, index = 1)
    private String userId;
    /** 用户姓名*/
    @ExcelProperty(value = {"姓名"}, index = 2)
    private String name;
    @ExcelProperty(value = {"性别"}, index = 3)
    private String gender;
    @ExcelProperty(value = {"民族"}, index = 4)
    private String nation;
    @ExcelProperty(value = {"年龄"}, index = 5)
    private Integer age;
    @ExcelProperty(value = {"婚姻状况"}, index = 6)
    private String maritalStatus;
    @ExcelProperty(value = {"籍贯"}, index = 7)
    private String nativePlace;
    @ExcelProperty(value = {"报道时间"}, index = 8)
    private Date registerTime;
    @ExcelProperty(value = {"最低期望薪资"}, index = 9)
    private Double expectedLowSalary;
    @ExcelProperty(value = {"最高期望薪资"}, index = 10)
    private Double expectedHighSalary;
    @ExcelProperty(value = {"最高学历"}, index = 11)
    private String highestEducation;
    @ExcelProperty(value = {"毕业学校"}, index = 12)
    private String graduationSchool;
    @ExcelProperty(value = {"毕业时间"}, index = 13)
    private Date graduationTime;
    @ExcelProperty(value = {"毕业专业"}, index = 14)
    private String graduationMajor;
    @ExcelProperty(value = {"联系方式"}, index = 15)
    private String contInfo;
    @ExcelProperty(value = {"紧急联系人"}, index = 16)
    private String emgeContactPerson;
    @ExcelProperty(value = {"紧急联系人联系方式"}, index = 17)
    private String emgePersonContactInfo;
    @ExcelProperty(value = {"身份证号码"}, index = 18)
    private String identityCard;
    @ExcelProperty(value = {"先居住地"}, index = 19)
    private String currentAddress;
    @ExcelProperty(value = {"微信号"}, index = 20)
    private String wechat;
    @ExcelProperty(value = {"E-mail"}, index = 21)
    private String email;
    @ExcelProperty(value = {"咨询师"}, index = 22)
    private String consultant;
    @ExcelProperty(value = {"班主任"}, index = 23)
    private String headmaster;
    @ExcelProperty(value = {"创建时间"}, index = 24)
    private String createTime;
    @ExcelProperty(value = {"更新时间"}, index = 25)
    private String updateTime;
}
