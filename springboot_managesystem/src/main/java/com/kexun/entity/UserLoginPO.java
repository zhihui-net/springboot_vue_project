package com.kexun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description: 登录信息
 * date 2023/1/15 15:15
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("user_login")
public class UserLoginPO {
    @TableId(value = "log_id",type = IdType.AUTO)
    private Integer logId;
    private String username;
    private String password;
    private String telNumber;
    private String role;
    private String createTime;
    private String updateTime;
}
