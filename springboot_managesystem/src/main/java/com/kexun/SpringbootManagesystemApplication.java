/*
*Copyright (c) 2022 nanjing kexun IT co.ltd.南京科迅科技有限公司
*/
package com.kexun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootManagesystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootManagesystemApplication.class, args);
        System.out.println();
    }
}
