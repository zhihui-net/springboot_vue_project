/*
*Copyright (c) 2022 nanjing kexun IT co.ltd.南京科迅科技有限公司
*/
package com.kexun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kexun.entity.UserInfoPO;

import java.util.List;

/**
 * Description:
 * date 2023/1/15 14:06
 *
 * @author SunQiang
 * @since JDK 1.8
 */
public interface UserInfoService extends IService<UserInfoPO> {
}
