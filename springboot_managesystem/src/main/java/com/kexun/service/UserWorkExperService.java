package com.kexun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kexun.entity.UserWorkExperPO;

public interface UserWorkExperService extends IService<UserWorkExperPO> {
}
