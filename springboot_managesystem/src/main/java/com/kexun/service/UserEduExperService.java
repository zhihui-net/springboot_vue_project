package com.kexun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kexun.entity.UserEduExperPO;

public interface UserEduExperService extends IService<UserEduExperPO> {
}
