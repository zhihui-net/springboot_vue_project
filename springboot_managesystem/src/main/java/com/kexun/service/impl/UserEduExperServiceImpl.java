package com.kexun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kexun.dao.UserEduExperMapper;
import com.kexun.entity.UserEduExperPO;
import com.kexun.service.UserEduExperService;
import org.springframework.stereotype.Service;

@Service
public class UserEduExperServiceImpl extends ServiceImpl<UserEduExperMapper, UserEduExperPO> implements UserEduExperService {
}
