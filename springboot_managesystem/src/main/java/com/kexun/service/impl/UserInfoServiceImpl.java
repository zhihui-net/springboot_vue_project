package com.kexun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kexun.dao.UserInfoMapper;
import com.kexun.entity.UserInfoPO;
import com.kexun.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Description:
 * date 2023/1/15 14:07
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfoPO> implements UserInfoService {

}
