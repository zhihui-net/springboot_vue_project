package com.kexun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kexun.dao.UserOtherMesgMapper;
import com.kexun.entity.UserOtherMesgPO;
import com.kexun.service.UserOtherMesgService;
import org.springframework.stereotype.Service;

@Service
public class UserOtherMesgServiceImpl extends ServiceImpl<UserOtherMesgMapper, UserOtherMesgPO> implements UserOtherMesgService {
}
