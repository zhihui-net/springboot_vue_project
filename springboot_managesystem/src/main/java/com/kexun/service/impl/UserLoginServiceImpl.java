package com.kexun.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kexun.dao.UserLoginMapper;
import com.kexun.entity.UserLoginPO;
import com.kexun.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description:
 * date 2023/1/15 15:21
 *
 * @author SunQiang
 * @since JDK 1.8
 */
@Service
public class UserLoginServiceImpl extends ServiceImpl<UserLoginMapper, UserLoginPO> implements UserLoginService {
@Autowired
private UserLoginMapper userLoginMapper;
    @Override
    public UserLoginPO findByUsername(String username) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("username",username);
        return userLoginMapper.selectOne(wrapper);
    }
}
