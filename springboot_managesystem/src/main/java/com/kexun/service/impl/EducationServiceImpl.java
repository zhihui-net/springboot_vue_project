package com.kexun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kexun.dao.EducationMapper;
import com.kexun.entity.UserEduExperPO;
import com.kexun.service.EducationService;
import org.springframework.stereotype.Service;

@Service
public class EducationServiceImpl
        extends ServiceImpl<EducationMapper, UserEduExperPO> implements EducationService {
}
