package com.kexun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kexun.dao.UserWorkExperMapper;
import com.kexun.entity.UserWorkExperPO;
import com.kexun.service.UserWorkExperService;
import org.springframework.stereotype.Service;

@Service
public class UserWorkExperServiceImpl extends ServiceImpl<UserWorkExperMapper, UserWorkExperPO> implements UserWorkExperService {
}
