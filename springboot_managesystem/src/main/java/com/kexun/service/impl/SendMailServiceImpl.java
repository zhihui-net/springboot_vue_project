package com.kexun.service.impl;

import com.kexun.service.SendMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @Author SunQiang
 * 2022/11/16
 * 22:24
 */
@Service
public class SendMailServiceImpl implements SendMailService {
    public static final int INT = 6;
    @Autowired
    private JavaMailSender javaMailSender;
    private String from = "sunqiang.loop@foxmail.com";

    /**
     * Description: 注册验证码
     * date: 2023/1/31 20:01
     *
     * @author sq
     * @since JDK 1.8
     */
    @Override
    public SimpleMailMessage sendMail(String mail) {
        String geycode = geycode();
        String context = "尊敬的用户您本次用于注册的验证码为:" + geycode + "。验证码提供给他人可能导致账号被盗，请勿泄露，谨防被骗。";
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(mail);
        message.setSubject(subject);
        message.setText(context);
        javaMailSender.send(message);
        System.out.println(message);
        return message;
    }

    private final ArrayList list = new ArrayList<>();
    //将产生的随机数，作为验证码(此处建议转成String类型)

    private String subject = "腾讯科技";

    /**
     * Description: 产生随机6位验证码
     * date: 2023/2/2 20:04
     *
     * @author sq
     * @since JDK 1.8
     */
    private String geycode() {
        StringBuilder bf = new StringBuilder();
        Collections.addAll(list, "1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
        for (int i = 0; i < INT; i++) {
            Collections.shuffle(list);
            bf.append(list.get(1));
        }
        return bf.toString();
    }

    //复杂邮件功能暂时用不到
    @Override
    public void sendExMail() {
/*        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper help = new MimeMessageHelper(message,true);
            String contextHtml = "<img src='https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg95.699pic.com%2Fphoto%2F50166%2F0846.jpg_wh300.jpg&refer=http%3A%2F%2Fimg95.699pic.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1671073367&t=eab47eac615239e09c1ac89de986c684' ></img> ";
            //附件
            File file = new File("E:\\redis.ico");
            help.setFrom(from);
            help.setTo(to);
            help.setSubject(subject);
            //添加附件
            help.setText(contextHtml ,true);
            help.setCc(to);
            help.addAttachment("redis.io",file);
            javaMailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }*/
    }
}
