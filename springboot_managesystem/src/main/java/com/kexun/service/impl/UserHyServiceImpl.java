package com.kexun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kexun.dao.UserHyMapper;
import com.kexun.entity.UserHyPO;
import com.kexun.service.UserHyService;
import org.springframework.stereotype.Service;

@Service
public class UserHyServiceImpl extends ServiceImpl<UserHyMapper, UserHyPO> implements UserHyService {
}
