package com.kexun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kexun.entity.UserInfoPO;
import com.kexun.entity.UserLoginPO;

/**
 * Description:
 * date 2023/1/15 15:20
 *
 * @author SunQiang
 * @since JDK 1.8
 */
public interface UserLoginService extends IService<UserLoginPO> {
    public UserLoginPO findByUsername(String username);
}
