package com.kexun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kexun.entity.UserOtherMesgPO;

public interface UserOtherMesgService extends IService<UserOtherMesgPO> {
}
