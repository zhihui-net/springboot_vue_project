package com.kexun.service;

import org.springframework.mail.SimpleMailMessage;

/**
 * @Author SunQiang
 * 2022/11/16
 * 22:23
 */
public interface SendMailService {
    //用户输入的邮箱号
    SimpleMailMessage sendMail(String mail);

    void sendExMail();
}
