package com.kexun.utils;

/**
* Description: 设置data为对象类型
* date 2023/2/4 21:39
* @author SunQiang					
* @since JDK 1.8						
*/	
public class Result3Utils {
    /**执行结果标记：true成功，false失败或者内部逻辑异常*/
    private Boolean flag = true;
    /**返回结果数据*/
    private Object data;

    public Result3Utils() {
    }

    public Result3Utils(Boolean flag, Object data) {
        this.flag = flag;
        this.data = data;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Result3Utils{" +
                "flag=" + flag +
                ", data=" + data +
                '}';
    }
}
