package com.kexun.utils;

public class SatokenInfo {
    /** 登录状态码 */
    public Integer status;
    /** 此token是否已经登录 */
    public Boolean isLogin;
    /** token数据 */
    public Object token;

    public SatokenInfo() {
    }

    public SatokenInfo(Integer status, Boolean isLogin, Object token) {
        this.status = status;
        this.isLogin = isLogin;
        this.token = token;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getLogin() {
        return isLogin;
    }

    public void setLogin(Boolean login) {
        isLogin = login;
    }

    public Object getToken() {
        return token;
    }

    public void setToken(Object token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "SatokenInfo{" +
                "status=" + status +
                ", isLogin=" + isLogin +
                ", token=" + token +
                '}';
    }
}
