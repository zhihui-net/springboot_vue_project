package com.kexun.utils;

import java.util.Map;

public class Result2Utils {
    /**执行结果标记：true成功，false失败或者内部逻辑异常*/
    private Boolean flag = true;
    /**返回结果数据*/
    private Map<String,Object> data;

    public Result2Utils() {
    }

    public Result2Utils(Boolean flag, Map<String, Object> data) {
        this.flag = flag;
        this.data = data;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Result2Utils{" +
                "flag=" + flag +
                ", data=" + data +
                '}';
    }
}
