/*
*Copyright (c) 2022 nanjing kexun IT co.ltd.南京科迅科技有限公司
*/
package com.kexun.utils;

import java.util.List;

/**
 * Description:  Result表现层一致的包装类
 * date 2022/12/30 14:03
 *
 * @author SunQiang
 * @since JDK 1.8
 */
public class ResultUtils<T> {
    /**执行结果标记：true成功，false失败或者内部逻辑异常*/
    private Boolean flag = true;
    /**返回结果数据*/
    private List<T> data;

    public ResultUtils() {
    }

    public ResultUtils(Boolean flag) {
        this.flag = flag;
    }

    public ResultUtils(Boolean flag, List<T> data) {
        this.flag = flag;
        this.data = data;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResultUtils{" +
                "flag=" + flag +
                ", data=" + data +
                '}';
    }
}
