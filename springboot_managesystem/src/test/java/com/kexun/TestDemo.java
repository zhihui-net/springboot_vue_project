package com.kexun;

import com.alibaba.fastjson.JSON;
import com.kexun.entity.UserInfoPO;
import com.kexun.service.UserInfoService;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class TestDemo {
    @Autowired
    private UserInfoService userInfoService;

    @Resource
    private RestHighLevelClient restHighLevelClient;

    @Test
    void addIndex() throws IOException {
        //创建索引
        CreateIndexRequest sqls = new CreateIndexRequest("demo");
        //执行请求
        CreateIndexResponse createIndexResponse = restHighLevelClient.indices().create(sqls, RequestOptions.DEFAULT);
        System.out.println(createIndexResponse);
    }


    //测试索引的获取,只能判断其存不存在
    @Test
    void testget() throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest("demo");
        boolean exists = restHighLevelClient.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
        System.out.println(exists);
    }



    @Test
    void testBulkRequest() throws IOException {
        BulkRequest bulkRequest = new BulkRequest();
        BulkRequest timeout = bulkRequest.timeout("10s");

        //批量插入数据
        List<UserInfoPO> list = userInfoService.getBaseMapper().selectList(null);

        for (int i = 0; i < list.size(); i++) {
            bulkRequest.add(
                    new IndexRequest("demo")
                            //自定义生成id，方便排序
                            .id("" + (i + 1))
                            .source(JSON.toJSONString(list.get(i)), XContentType.JSON));
        }
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println(bulk.hasFailures());//是否失败，false代表插入成功了
    }

    //查询
    //SearchRequest 搜索请求
    //SearchSourceBuilder 条件构造
    @Test
    void configtionSearch() throws IOException {
        SearchRequest searchRequest = new SearchRequest("demo");
        //构造搜索条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //查询条件，我们可以使用QueryBuilders工具类
        //QueryBuilders.termQuery  精确查询
        //QueryBuilders.matchAllQuery() 匹配所有
//        TermQueryBuilder name = QueryBuilders.termQuery("name", "omt");
        MatchQueryBuilder name = QueryBuilders.matchQuery("name", "tom");
        searchSourceBuilder.query(name);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        searchRequest.source(searchSourceBuilder);

        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        System.out.println(JSON.toJSONString(search.getHits()));
        System.out.println("==========");
        for (SearchHit document : search.getHits().getHits()){
            System.out.println(document.getSourceAsMap());
        }
    }


    @Test
    void subStringss(){
        String s = "尊敬的用户您本次用于注册的验证码为:123456后面没有了";
        System.out.println(s.substring(18,24));
    }
}
