package com.kexun;

import com.alibaba.excel.EasyExcel;
import com.kexun.entity.UserInfoPO;
import com.kexun.service.UserInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestExcel {
    @Autowired
    private UserInfoService userInfoService;

    @Test
    void method(){
        List<UserInfoPO> list = userInfoService.getBaseMapper().selectList(null);
        String PATH = "E:\\ExcelDown\\";
        String fileName = PATH + "EastExcel.xlsx";
        EasyExcel.write(fileName,UserInfoPO.class).sheet("模板").doWrite(list);
    }
}
