package com.kexun;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kexun.dao.UserInfoMapper;
import com.kexun.entity.UserInfoPO;
import com.kexun.entity.UserLoginPO;
import com.kexun.service.SendMailService;
import com.kexun.service.UserInfoService;
import com.kexun.service.UserLoginService;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class SpringbootManagesystemApplicationTests {
    @Autowired
    UserInfoService userInfoService;

    @Autowired
    UserLoginService userLoginService;

    @Autowired
    private SendMailService sendMailService;

    /**
     * Description: 测试发送随机验证码邮件                        
     * date: 2023/1/29 21:54
                             
     * @author sq
     * @since JDK 1.8                        
     */   
//    @Test
//    void contextLoads(String mail) {
//        sendMailService.sendMail();
//    }

    @Test
    void testm() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "tom");
        List<UserInfoPO> userInfoPOS = userInfoService.getBaseMapper().selectByMap(map);
        for (UserInfoPO userInfoPO : userInfoPOS) {
            System.out.println(userInfoPO);
        }
    }


    //测试查询用户
    @Test
    void contextLoadssfss() {
        List<UserInfoPO> userInfoPOS = userInfoService.getBaseMapper().selectList(null);
        for (UserInfoPO userInfoPO : userInfoPOS) {
            System.out.println(userInfoPO);
        }
    }

    //测试用户登录
    @Test
    void Loginuser() {
        Map<String, Object> map = new HashMap<>();
        map.put("username", "hello");
        map.put("password", "12345");
        //创建对象
//        QueryWrapper<UserLoginPO> wrapper = new QueryWrapper<>();
//        //通过wrapper设置条件。
//        wrapper.eq("",);
        List<UserLoginPO> userLoginPOS = userLoginService.getBaseMapper().selectByMap(map);
        System.out.println(userLoginPOS);
    }


    /**
     * Description: 测试用户注册
     * date: 2023/1/27 20:47

     * @author sq
     * @since JDK 1.8
     */
    @Test
    void testpassword(){

    }



}
