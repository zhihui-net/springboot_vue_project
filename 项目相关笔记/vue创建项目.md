# Vue创建项目以及出现的问题

### 1、进入需要创建项目的目录

vue create 项目名称



### 2、vue2为例

![image-20230116155613333](vue创建项目.assets/image-20230116155613333.png)

回车确定



### 3、项目创建完成

![image-20230116155744996](vue创建项目.assets/image-20230116155744996.png)



### 4、进入项目

![image-20230116155831708](vue创建项目.assets/image-20230116155831708.png)



### 5、运行项目

![image-20230116155913733](vue创建项目.assets/image-20230116155913733.png)

选择local地址，浏览器打开。





### 6、vue创建项目遇到的问题

## 1、axios在vue2中需要在main.js中导入。直接在组件中使用。

不然会出现模块找不到。



2、引入router

![image-20230116211950572](vue创建项目.assets/image-20230116211950572.png)



## 2、登录功能实现过程中出现的错误

axios发送get请求的话，此处我是vue3项目，vue2和vue3存在写法上的不同。

下载axios之后，在main.js中引入axios

```vue
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//引入组件
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
//引入axios组件
import axios from 'axios';

import VueAxios from 'vue-axios'

//引入定义的路由器
// Vue.prototype.$axios = axios


createApp(App).use(store).use(VueAxios,axios).use(router).use(ElementPlus).mount('#app')

```





正确代码：

1、 在给登录按钮绑定点击事件。

```
<!-- 用户名密码及登录按钮部分 -->
        <div class="form">
          <div class="user1">
          <!-- vue的v-model双向绑定数据 -->
            <input type="text" v-model="username" placeholder="用户名">
          </div>
          <div class="user2">
            <input type="password" v-model="password" placeholder="密码">
          </div>
          <div>
          <!-- vue3绑定事件写法 -->
            <button type="button" @:click="login">登录</button>
          </div>
        </div>
```

2、调用login()方法

```vue
methods: {
    login(){
      let that = this
      //此处vue3写法不需要加$,不同于vue2
      this.axios.get('http://localhost:8088/sq/user/login',{
        params: {
          username: this.username,
          password: this.password
        }
      }).then(function (res){
        console.log(res);
        if(res.data === '登录成功'){
          //vue3路由跳转写法,此处应该使用that，原理暂时不清楚
          that.$router.push("/home")
        }else{
          console.log("登录信息不正确...")
        }
      }).catch(function(error){
        if(error){
          console.log('登录失败...')
        }
      })
    }
```



## 3、注册功能出现的问题





正确代码：

```
 methods:{

    register(){
      let that = this
      //此处vue3写法不需要加$,不同于vue2
      this.axios.post('http://localhost:8088/sq/user',{
          username: this.username,
          password: this.password,
          telNumber: this.telNumber
      }).then(function (res){
          //vue3路由跳转写法,此处应该使用that，原理暂时不清楚
          that.$router.push("/")
      })
    }

  }
```



## 4、使用element-plus代码出现的问题

描述：官网复制的代码，中到自己页面中出现错误



解决办法：

![image-20230120101007894](vue创建项目.assets/image-20230120101007894.png)



删掉lang="js",即可

如果还不行可以下载这个依赖

![image-20230120101112792](vue创建项目.assets/image-20230120101112792.png)

在main.js中引用这个依赖(根据需要，我没有引用)

```js
//引入icon
import * as Elicons from "@element-plus/icons-vue";
const app = createApp(App);
for (const name in Elicons) {
    app.component(name, Elicons[name]);
}
```
