# Sa-token使用

### 1、导入依赖

```xml
     <!--so-token依赖-->
        <dependency>
            <groupId>cn.dev33</groupId>
            <artifactId>sa-token-spring-boot-starter</artifactId>
            <version>1.28.0</version>
        </dependency>
```

### 2、配置sa-token

```yml
# Sa-Token配置
sa-token:
  # token名称 (同时也是cookie名称)
  token-name: xc_satoken
  # token有效期，单位s 默认30天, -1代表永不过期
  timeout: 2592000
  # token临时有效期 (指定时间内无操作就视为token过期) 单位: 秒
  activity-timeout: -1
  # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
  is-concurrent: true
  # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)
  is-share: false
  # token风格
  token-style: uuid
  # 是否输出操作日志
  is-log: false
```

### 3、编写带有token信息的工具类

```java
package com.kexun.utils;

public class SatokenInfo {
    /** 登录状态码 */
    public Integer state;
    /** 此token是否已经登录 */
    public Boolean isLogin;
    /** 自定义数据 */
    public Object msg;

    public SatokenInfo() {
    }

    public SatokenInfo(Integer state, Boolean isLogin, Object msg) {
        this.state = state;
        this.isLogin = isLogin;
        this.msg = msg;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Boolean getLogin() {
        return isLogin;
    }

    public void setLogin(Boolean login) {
        isLogin = login;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "SatokenInfo{" +
                "state=" + state +
                ", isLogin=" + isLogin +
                ", msg=" + msg +
                '}';
    }
}
```

### 4、controller层测试类

```java
package com.kexun.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import com.kexun.utils.SatokenInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//此处由于可能是返回的是一个对象，所以不能使用text/html
@RequestMapping(value = "/test", produces = "application/json; charset=utf-8")
public class TestController {

    @PostMapping("/login")
    public String login(String username, String password) {
        //模拟登录校验
        if ("sq".equals(username) && "123456".equals(password)) {
            StpUtil.login(1001);
            return "登陆成功";
        }
        return "登陆失败";
    }
    @GetMapping("/token")
    public SatokenInfo login() {
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        System.out.println(tokenInfo);
        SatokenInfo satokenInfo = new SatokenInfo();
        satokenInfo.setMsg(StpUtil.getTokenValue());
        satokenInfo.setState(200);
        System.out.println("输出结果："+satokenInfo);
        return satokenInfo;
    }
}
```



### 5、postman测试的话先执行第一个方法，让账户登录，然后在确认token信息

登录：

![image-20230121224959594](Sa-token使用.assets/image-20230121224959594.png)



获取token信息：

![image-20230121225033965](Sa-token使用.assets/image-20230121225033965.png)







### 6、补充：Sa-token地址：[登录认证 - Sa-Token](https://sa-token.cc/doc.html#/use/login-auth)

查询token信息：

```java
// 获取当前会话的token值
StpUtil.getTokenValue();

// 获取当前`StpLogic`的token名称
StpUtil.getTokenName();

// 获取指定token对应的账号id，如果未登录，则返回 null
StpUtil.getLoginIdByToken(String tokenValue);

// 获取当前会话剩余有效期（单位：s，返回-1代表永久有效）
StpUtil.getTokenTimeout();

// 获取当前会话的token信息参数
StpUtil.getTokenInfo();
```





### 待更新.....
