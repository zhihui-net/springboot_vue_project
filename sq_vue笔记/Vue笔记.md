# Vue笔记

网络通信： axios

页面跳转： vue-router

状态管理： vuex

Vue-UI：ICE



## 1、第一个程序

```html
<!DOCTYPE html>
<html lang="en">

<head>
    hello2
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>


    <div id="app">
        <h2>{{message}}</h2>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <script>
        var vm = new Vue({
            el: "#app",
            //Model: 数据
            data: {
                message: "hello vue!"
            }
        });
    </script>
</body>

</html>
```



页面显示：

![image-20221231194125027](Vue笔记.assets/image-20221231194125027.png)

动态修改模板





### 1、v-bind绑定

v-bind绑定称为指令，前缀v- 表示Vue提供的指令。作用是渲染Dom

该指令含义是将元素节点的title特性和Vue实例的message属性保持一致

```
    <span v-bind:title="message">
            悬停几秒
        </span>
```



### 2、v-if

 v-if 指令将根据表达式 seen 的值(true 或 false )来决定是否插入 p 元素。

```html
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Vue 测试实例 - 菜鸟教程(runoob.com)</title>
<script src="https://cdn.staticfile.org/vue/2.2.2/vue.min.js"></script>
</head>
<body>
<div id="app">
    <p v-if="seen">现在你看到我了</p>
    <template v-if="ok">
      <h1>菜鸟教程</h1>
      <p>学的不仅是技术，更是梦想！</p>
      <p>哈哈哈，打字辛苦啊！！！</p>
    </template>
</div>
    
<script>
new Vue({
  el: '#app',
  data: {
    seen: true,
    ok: false
  }
})
</script>
</body>
</html>
```



### 3、v-on

它用于监听 DOM 事件：



### 4、**v-model** 

**v-model** 指令用来在 input、select、textarea、checkbox、radio 等表单控件元素上创建双向数据绑定，根据表单上的值，自动更新绑定的元素的值。





## 2、学习结构

#### 1、循环-判断

1、判断（if-else）

```js
 <div id="app">
        <h1 v-if="ok">Yes</h1>
        <h1 v-else>No</h1>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <script>
        var vm = new Vue({
            el: "#app",
            //Model: 数据
            data: {
                ok: true
            }
        });
    </script>
```

2、if-else-if

```javaScript
  <div id="app">
        <h1 v-if="type==='A'">A</h1>
        <h1 v-else-if="type==='B'">B</h1>
        <h1 v-else-if="type==='C'">C</h1>
        <h1 v-else="type==='D'">D</h1>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <script>
        var vm = new Vue({
            el: "#app",
            //Model: 数据
            data: {
                type: 'A'
            }
        });
    </script>
```



#### 2、事件

浏览器事件： window     document

Dom事件： 增  删  遍历  修改节点元素内容



JQuery



#### 3、视图渲染

html  

css

#### 4、通信

JQuery   ----Ajax





## 3、vue项目基础

### 1、一个容器只能被一个vue实例接管  （两者关系只能是一对一）

![image-20230113094421948](Vue笔记.assets/image-20230113094421948.png)



### 2、指令和插值语法

![image-20230113101008442](Vue笔记.assets/image-20230113101008442.png)



V-bind（可以简写成 ：）和差值表达式{{}} 的应用1：当1和2两处的name重名时可以考虑。适当的将属性变成一个对象形式。

![image-20230113101833349](Vue笔记.assets/image-20230113101833349.png)



### 3、数据绑定

#### 1、单向绑定数据：

![image-20230113103340732](Vue笔记.assets/image-20230113103340732.png)



#### 2、双向绑定：（vue实例和容器数据发生同步变化）

![image-20230113103538265](Vue笔记.assets/image-20230113103538265.png)



#### 3、v-model和v-bind区别总结：

![image-20230113104126770](Vue笔记.assets/image-20230113104126770.png)

#### 4、v-model和v-bind简写形式：

![image-20230113104535674](Vue笔记.assets/image-20230113104535674.png)



### 4、el和date

el两种写法（可以任意选择）

![image-20230113111100298](Vue笔记.assets/image-20230113111100298.png)

date两种写法：（推荐使用第二种写法）

![image-20230113110940022](Vue笔记.assets/image-20230113110940022.png)



el和date小结

![image-20230113111204611](Vue笔记.assets/image-20230113111204611.png)  



### 5、MVVM理解

![image-20230113112512977](Vue笔记.assets/image-20230113112512977.png)









#### 基础内容待补充......













## 4、vue中写html文件的问题

vue中是单页面配置的，即是只写一个index页面。其他通过组件的形式，来完成功能的补充。

以往的html页面是通过多个页面跳转的方式来完成。vue则是通过路由的方式。





## 5、前端发送请求，后端处理请求

### 1、postmam模拟前端发送的请求

postman模拟前端发送请求

![image-20230113214432823](Vue笔记.assets/image-20230113214432823.png)



用controller层接受处理参数：

![image-20230113215848687](Vue笔记.assets/image-20230113215848687.png)





### 2、使用axios发送前端的请求步骤

参考文章1：

[Vue3使用axios的配置教程_夏日米米茶-DevPress官方社区 (csdn.net)](https://huaweicloud.csdn.net/638ee1c7dacf622b8df8d7e2.html?spm=1001.2101.3001.6661.1&utm_medium=distribute.pc_relevant_t0.none-task-blog-2~default~BlogCommendFromBaidu~activity-1-123682618-blog-117688019.pc_relevant_vip_default&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2~default~BlogCommendFromBaidu~activity-1-123682618-blog-117688019.pc_relevant_vip_default&utm_relevant_index=1)



参考文章2：

[(32条消息) 3分钟让你学会axios在vue项目中的基本用法（建议收藏）_柠檬树上柠檬果柠檬树下你和我的博客-CSDN博客](https://blog.csdn.net/qq_39765048/article/details/117688019?ops_request_misc=%7B%22request%5Fid%22%3A%22167361174616800222826623%22%2C%22scm%22%3A%2220140713.130102334..%22%7D&request_id=167361174616800222826623&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-117688019-null-null.142^v71^control_1,201^v4^add_ask&utm_term=Axios&spm=1018.2226.3001.4187)







## 6、文件上传案例

### 1、简单模拟

application.yml

```yml
spring:
  # 上传文件配置
  servlet:
    multipart:
      max-file-size: 100MB

#文件访问配置
file:
  path: /file/**  # 文件访问路径
  address: E://uploadFiles/  # 文件保存路径
```

注意点：path和address是属于同一级别的属性。



index.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
</head>
<body>
<div id="app">
    <el-upload
            class="upload-demo"
            action="/upload"
            :on-preview="handlePreview"
            accept=".png"
            :limit="10">
        <el-button size="small" type="primary">点击上传</el-button>
        <div slot="tip" class="el-upload__tip">只能上传png图片</div>
    </el-upload>
</div>


<script src="https://cdn.staticfile.org/vue/2.2.2/vue.min.js"></script>
<script src="https://unpkg.com/element-ui/lib/index.js"></script>
<script>
    new Vue({
        el:"#app",
        methods:{
            handlePreview(file) {
                window.open(file.response.url)
            }
        }
    })
</script>
</body>
</html>
```

==问题==：上传之后无法预览图片

接口文件

```java
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
public class UploadApi {
    @Value("${file.address}")
    private String address;
    private String path = "/file";
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @PostMapping("/upload")
    public Map<String, Object> upload(MultipartFile file, HttpServletRequest request) {
        Map<String, Object> result = new HashMap<>();
        String originName = file.getOriginalFilename();
        if (!originName.endsWith(".png")) {
            result.put("status", "error");
            result.put("msg", "文件类型不对");
            return result;
        }

        String format = sdf.format(new Date());
        String realPath = address + format;


        File folder = new File(realPath);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        String newName = UUID.randomUUID().toString() + ".png";

        try {
            file.transferTo(new File(folder, newName));
            String url = request.getScheme() + "://" + request.getServerName() + request.getServerPort() + path + format + newName;
            result.put("status", "success");
            result.put("url", url);

        } catch (IOException e) {
            e.printStackTrace();
            result.put("status", "error");
            result.put("msg", e.getMessage());
        }
        return result;
    }
}

```



配置类

```java
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class FilePathConfig implements WebMvcConfigurer {
    @Value("${file.path}")
    private String path;
    @Value("${file.address}")
    private String address;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(path).addResourceLocations("file"+address);
    }
}
```

效果

![image-20230114153242331](Vue笔记.assets/image-20230114153242331.png)





### 2、使用vue框架实现









## 7、Vue实现发送请求CRUD操作数据库（待更新......)

#### 1、后端代码：

1、配置跨域请求的配置类

```java
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                // 设置允许跨域请求的域名
                .allowedOriginPatterns("*")
                // 放行全部请求类型
//                .allowedMethods("*")
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                // 是否发送Cookie
                .allowCredentials(true)
                //暴露哪些头部信息
                .exposedHeaders("*");
    }
}
```



2、controller层暴露前台查询的访问路径 http://localhost:8090/sq/book

沿用Mybatis-Plus快速简化开发操作数据库代码（以查询所有数据为例）



#### 2、前端部分

搭建项目vue-cli项目（不同教程使用脚手架开发可能出现问题！）

1、BookManage组件

```vue
<template>
  <div>
    <table>
      <tr>
        <tb>编号</tb>
        <tb>名称</tb>
        <tb>作者</tb>
      </tr>
      <tr></tr>
      <!--将books中的数据循环遍历 -->
      <tr v-for="item in books">
        <td>{{ item.id }}</td>
        <td>{{ item.name }}</td>
        <td>{{ item.author }}</td>
      </tr>
    </table>
  </div>
</template>

<script>
export default {
  name: "Book",
  data() {
    return {
      books: [{
        id: 1,
        name: '解忧杂货店',
        author: '东野圭吾'
      }, {
        id: 2,
        name: '追风筝的人',
        author: '卡勒德·胡赛尼'
      },
       {
        id: 6,
        name: '人间失格',
        author: '太宰治'
      }
      ]
    }
  },
  created() {
     //注意点：此处将this提出function函数外后，this指代的擦拭vue对象实例。如果放在函数中，则不是vue对象了
    const _this = this
    //get或者其他请求方式，为前台访问的路径
    axios.get('http://localhost:8090/sq/book').then(function (resp) {
      console.log(resp)
       //将后台返回的结果赋值给当前vue对象的books数组中去
      _this.books = resp.data.data
    })
  }
}
</script>
```



2、下载组件

![image-20230114224420748](Vue笔记.assets/image-20230114224420748.png)

注意点：这里由于不同方式下载的脚手架，创建出来的项慕可能会存在差异，因此我在创建项目时，下载axios时会导致我无法启动vue项目。主页面显示为空。（该问题暂未解决）

目前只有这种形式的结构可以前后端联调。



3、main.js引入组件

![image-20230114225220049](Vue笔记.assets/image-20230114225220049.png)

==注意== 这里写法可能不太一样。



3、将BookManage引入路由index.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
//引入book组件
import BookManage from '../views/BookManage'


Vue.use(VueRouter)
const routes = [
    // 绑定book组件
  {
    path:"/bookManage",
    name:"图书管理",
    component: BookManage
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
```







结果图：

前台：

使用路径为localhost:8080/  +  注册组件时的名称（BookManage)

![image-20230114230025168](Vue笔记.assets/image-20230114230025168.png)



后台结果：

![image-20230114225920674](Vue笔记.assets/image-20230114225920674.png)
