-- MySQL dump 10.13  Distrib 5.7.38, for Win64 (x86_64)
--
-- Host: localhost    Database: managesystem
-- ------------------------------------------------------
-- Server version	5.7.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_edu_exper`
--

DROP TABLE IF EXISTS `user_edu_exper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_edu_exper` (
  `edu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` varchar(20) DEFAULT NULL COMMENT '学员编号',
  `start_date` date DEFAULT NULL COMMENT '开始时间',
  `end_date` date DEFAULT NULL COMMENT '终止时间',
  `school_or_institutions` varchar(50) DEFAULT NULL COMMENT '毕业院校或培训机构',
  `major` varchar(10) DEFAULT NULL COMMENT '专业',
  `educational_level` varchar(10) DEFAULT NULL COMMENT '学历层次',
  `create_time` date NOT NULL COMMENT '创建时间',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`edu_id`),
  KEY `scl_index` (`school_or_institutions`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_edu_exper`
--

LOCK TABLES `user_edu_exper` WRITE;
/*!40000 ALTER TABLE `user_edu_exper` DISABLE KEYS */;
INSERT INTO `user_edu_exper` VALUES (1,'2300001','2018-09-01','2022-07-01','南昌航空工业学院','大数据科学与技术','本科','2022-01-15',NULL),(2,'2300001','2022-09-01','2023-01-09','黑马程序员','java后端','','2022-01-15',NULL),(3,'2300002','2018-09-01','2022-07-01','山西中医学院','软件工程','本科','2022-01-15',NULL),(4,'2300002','2022-09-01','2023-01-09','黑马程序员','java后端','','2022-01-15',NULL),(5,'2300003','2018-09-01','2022-07-01','华东政法学院','软件工程','本科','2022-01-15',NULL),(6,'2300003','2022-09-01','2023-01-09','黑马程序员','java后端','','2022-01-15',NULL),(7,'231001',NULL,NULL,'北师大学院','电子信息与技术',NULL,'2022-12-12',NULL);
/*!40000 ALTER TABLE `user_edu_exper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_hy`
--

DROP TABLE IF EXISTS `user_hy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_hy` (
  `hy_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` varchar(20) NOT NULL COMMENT '学员编号',
  `hy_description` varchar(500) DEFAULT NULL COMMENT '患病描述',
  `illness_type` varchar(50) DEFAULT NULL COMMENT '疾病种类',
  `illness_status` varchar(10) DEFAULT NULL COMMENT '患病状态',
  `create_time` date NOT NULL COMMENT '创建时间',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`hy_id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_hy`
--

LOCK TABLES `user_hy` WRITE;
/*!40000 ALTER TABLE `user_hy` DISABLE KEYS */;
INSERT INTO `user_hy` VALUES (1,'2300001','有无以下病史','高血压','有','2023-01-15',NULL),(2,'2300001','有无过敏史',NULL,'无','2023-01-15',NULL),(3,'2300001','需要说明的特殊情况',NULL,'无','2023-01-15',NULL),(4,'2300001','有无发烧，呼吸道疾病',NULL,'无','2023-01-15',NULL),(5,'2300001','14天内是否接触发烧人群',NULL,'无','2023-01-15',NULL),(6,'2300001','14天内是否有从高风险地区旅居史',NULL,'无','2023-01-15',NULL),(7,'2300002','有无以下病史','','无','2023-01-15',NULL),(8,'2300002','有无过敏史',NULL,'无','2023-01-15',NULL),(9,'2300002','需要说明的特殊情况',NULL,'无','2023-01-15',NULL),(10,'2300002','有无发烧，呼吸道疾病',NULL,'无','2023-01-15',NULL),(11,'2300002','14天内是否接触发烧人群',NULL,'无','2023-01-15',NULL),(12,'2300002','14天内是否有从高风险地区旅居史',NULL,'无','2023-01-15',NULL);
/*!40000 ALTER TABLE `user_hy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '随机编号',
  `user_id` varchar(20) NOT NULL COMMENT '学员编号',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `gender` varchar(10) DEFAULT NULL COMMENT '性别',
  `nation` varchar(10) DEFAULT NULL COMMENT '名族',
  `age` int(10) DEFAULT NULL COMMENT '年龄',
  `marital_status` varchar(10) DEFAULT NULL COMMENT '婚姻状况',
  `native_place` varchar(100) DEFAULT NULL COMMENT '籍贯',
  `register_time` date DEFAULT NULL COMMENT '报到时间',
  `expected_low_salary` int(11) DEFAULT NULL COMMENT '期望最低薪资',
  `expected_high_salary` int(11) DEFAULT NULL COMMENT '期望最高薪资',
  `highest_education` varchar(10) DEFAULT NULL COMMENT '最高学历',
  `graduation_school` varchar(50) DEFAULT NULL COMMENT '毕业学校',
  `graduation_time` date DEFAULT NULL COMMENT '毕业时间',
  `graduation_major` varchar(10) DEFAULT NULL COMMENT '毕业专业',
  `cont_info` varchar(30) DEFAULT NULL COMMENT '联系方式',
  `emge_contact_person` varchar(100) DEFAULT NULL COMMENT '紧急联系人',
  `emge_person_contact_info` varchar(30) DEFAULT NULL COMMENT '紧急联系人联系方式',
  `identity_card` varchar(21) DEFAULT NULL COMMENT '身份证号码',
  `current_address` varchar(100) DEFAULT NULL COMMENT '现居住地',
  `wechat` varchar(30) DEFAULT NULL COMMENT '微信号',
  `email` varchar(150) DEFAULT NULL COMMENT 'E-mail',
  `consultant` varchar(100) DEFAULT NULL COMMENT '咨询师',
  `headmaster` varchar(100) DEFAULT NULL COMMENT '班主任',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `name_index` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES (1,'230001','tom','e','sdc',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'17702566308',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-12-12 00:00:00',NULL),(2,'230002','aaa','dc','sd',212,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1985634712',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-12-12 00:00:00',NULL),(3,'230003','bbb','sd','sdc',12,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-12-12 00:00:00',NULL),(4,'230004','ccc','sd','sc',34,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-12-12 00:00:00',NULL),(17,'231000','tom','男','北京',29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'231000','tom','男','北京',29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'231000','你好','男','北京',29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,'231000','你好','男','北京',29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,'231000','你好','男','北京',29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,'231000','你好','男','北京',29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,'231000','你好','男','北京',29,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login`
--

DROP TABLE IF EXISTS `user_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_login` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `username` varchar(20) NOT NULL COMMENT '注册的用户名',
  `password` varchar(35) NOT NULL COMMENT '注册的密码',
  `tel_number` varchar(20) NOT NULL COMMENT '注册时的手机号',
  PRIMARY KEY (`log_id`),
  UNIQUE KEY `user_login_username_IDX` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login`
--

LOCK TABLES `user_login` WRITE;
/*!40000 ALTER TABLE `user_login` DISABLE KEYS */;
INSERT INTO `user_login` VALUES (1,'hello','827ccb0eea8a706c4c34a16891f84e7b','17702566308'),(11,'sq','4bc92a7aeb9478e6bf3f989025232b22','1289468972'),(14,'hello1','827ccb0eea8a706c4c34a16891f84e7b','12342534567');
/*!40000 ALTER TABLE `user_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_other_mesg`
--

DROP TABLE IF EXISTS `user_other_mesg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_other_mesg` (
  `ot_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` varchar(20) NOT NULL COMMENT '学员编号',
  `english_ability` varchar(10) DEFAULT NULL COMMENT '英语等级能力',
  `other_language` varchar(10) DEFAULT NULL COMMENT '其他语种',
  `other_language_ability` varchar(10) DEFAULT NULL COMMENT '其他语言能力',
  `require_computer` varchar(10) DEFAULT NULL COMMENT '是否需要领用电脑',
  `obtain_employment` varchar(10) DEFAULT NULL COMMENT '是否需要就业推荐',
  `arrange_accommodation` varchar(10) DEFAULT NULL COMMENT '是否需要安排住宿',
  `registration_attachment` varchar(15) NOT NULL COMMENT '报名附件',
  `signature_of_trainees` varchar(100) DEFAULT NULL COMMENT '学员签字',
  `filling_time` date DEFAULT NULL COMMENT '填表时间',
  `create_time` date NOT NULL COMMENT '创建时间',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ot_id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_other_mesg`
--

LOCK TABLES `user_other_mesg` WRITE;
/*!40000 ALTER TABLE `user_other_mesg` DISABLE KEYS */;
INSERT INTO `user_other_mesg` VALUES (1,'230001',NULL,NULL,NULL,NULL,NULL,NULL,'2022-11-11',NULL,NULL,'2022-11-11',NULL),(2,'230002',NULL,NULL,NULL,NULL,NULL,NULL,'2022-11-11',NULL,NULL,'2022-11-11',NULL);
/*!40000 ALTER TABLE `user_other_mesg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_work_exper`
--

DROP TABLE IF EXISTS `user_work_exper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_work_exper` (
  `work_id` int(11) NOT NULL COMMENT '编号',
  `user_id` varchar(20) DEFAULT NULL COMMENT '学员编号',
  `start_time` date DEFAULT NULL COMMENT '开始时间',
  `end_time` date DEFAULT NULL COMMENT '终止时间',
  `company` varchar(50) DEFAULT NULL COMMENT '单位名称',
  `major` varchar(10) DEFAULT NULL COMMENT '职务',
  `about_course` varchar(10) DEFAULT NULL COMMENT '是否和所学课程相关',
  `create_time` date NOT NULL COMMENT '创建时间',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`work_id`),
  KEY `com_index` (`company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_work_exper`
--

LOCK TABLES `user_work_exper` WRITE;
/*!40000 ALTER TABLE `user_work_exper` DISABLE KEYS */;
INSERT INTO `user_work_exper` VALUES (1,'230001',NULL,NULL,NULL,NULL,NULL,'2022-06-06',NULL),(2,'230002',NULL,NULL,NULL,NULL,NULL,'2022-06-06',NULL);
/*!40000 ALTER TABLE `user_work_exper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'managesystem'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-28 15:15:40
